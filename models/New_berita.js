const mongoose = require("mongoose")
const schema = mongoose.Schema({
    title: String,
    content: String,
    gambar: String,
    link: String,
})
module.exports = mongoose.model("New_berita", schema)