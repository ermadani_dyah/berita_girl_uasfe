const mongoose = require("mongoose")
const schema = mongoose.Schema({
    title: String,
    gambar: String,
    link: String,
    tanggal: String,
})
module.exports = mongoose.model("Trending_berita", schema)