const mongoose = require("mongoose")
const schema = mongoose.Schema({
    title: String,
    content: String,
    gambar: String,
    link: String,
})
module.exports = mongoose.model("Top_Week_berita", schema)