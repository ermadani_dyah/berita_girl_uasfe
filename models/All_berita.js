const mongoose = require("mongoose")
const schema = mongoose.Schema({
    title: String,
    content: String,
    gambar: String,
    link: String,
    tanggal: String,
    kategori: String,
})
module.exports = mongoose.model("All_berita", schema)