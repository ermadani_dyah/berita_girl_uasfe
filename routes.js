const express = require("express")
const All = require("./models/All_berita") // ALL BERITA
const Trending = require("./models/Trending_berita")//TRENDING BERITA
const New = require("./models/New_berita")//NEW BERITA
const Top_week = require("./models/Top_week_berita")//TOP WEEK BERITA
const Auth = require("./models/Auth") //LOGIN
const router = express.Router()
var path = require('path');
var jwt = require('jsonwebtoken');



// function untuk mengecek token
function isAuthenticated(req, res, next){
	var token = req.header('auth-token') ||req.params.id; //req.body.token || req.query.token || req.headers.authorization; // mengambil token di antara request
	if(token){ //jika ada token
	    jwt.verify(token, 'jwtsecret', function(err, decoded){ //jwt melakukan verify
            if (err) { // apa bila ada error
                res.json({message: 'Failed to authenticate token'}); // jwt melakukan respon
            }else { // apa bila tidak error
                req.decoded = decoded; // menyimpan decoded ke req.decoded
                next(); //melajutkan proses
            }
	    });
	}else { // apa bila tidak ada token
	    return res.status(403).send({message: 'No token provided.'}); // melkukan respon kalau token tidak ada
	}
}
// function untuk refresh token
router.post("/refresh_token", async (req, res) => {
	var last_username=req.body.username;
	var last_token=req.body.last_token;
	jwt.verify(last_token, 'jwtsecret', function(err, decoded){ //jwt melakukan verify
		if (err) { // apa bila ada error
		    res.json({message: 'Failed to authenticate token'}); // jwt melakukan respon
		}else { // apa bila tidak error
		    req.decoded = decoded; // menyimpan decoded ke req.decoded
		    // terbitkan token baru
		    var token = jwt.sign({last_username}, 'jwtsecret', {algorithm: 'HS256', expiresIn:'10s'});
		    return res.status(200).json({
                token:token,
                status: res.statusCode,
                message: 'Token Baru!'
		    })
		}
	})
})
//-->LOGIN<--//
router.post("/login_berita", async (req, res) => {
	const user = await Auth.findOne({username: req.body.username,pass: req.body.pass})
    if(!user) return res.status(400).json({
        status: res.statusCode,
        message: 'Anda Gagal Login'
    })
    else
        var token = jwt.sign({username: req.body.username}, 'jwtsecret', {algorithm: 'HS256', expiresIn:'10s'}); //token expire dalam 10 detik
        return res.status(200).json({
            token:token,
            username:req.body.username,
            status: res.statusCode,
            message: 'yee sukses login'
        })
})

//-->ALL BERITA<--//
// Get All Berita
router.get("/all_berita", isAuthenticated, async (req, res, next) => {
    const all_berita = await All.find()
    res.send(all_berita)
})
//Post All berita
router.post("/all_berita", async (req, res) => {
    const all_berita = new All({
        title: req.body.title,
        content: req.body.content,
        gambar: req.body.gambar,
        link: req.body.link,
        tanggal: req.body.tanggal,
        kategori: req.body.kategori,
    })
    await all_berita.save()
    res.send(all_berita)
})
//Update All berita
router.patch("/all_berita/:id", async (req, res) => {
    try {
        const all_berita = await All.findOne({ _id: req.params.id })
        if (req.body.title) {
            all_berita.title = req.body.title
        }
        if (req.body.content) {
            all_berita.content = req.body.content
        }
        if (req.body.gambar) {
            all_berita.gambar = req.body.gambar
        }
        if (req.body.link) {
            all_berita.link = req.body.link
        }
        if (req.body.tanggal) {
            all_berita.tanggal = req.body.tanggal
        }
        if (req.body.kategori) {
            all_berita.kategori = req.body.kategori
        }
        await all_berita.save()
        res.send(all_berita) 
    }catch {
        res.status(404)
        res.send({ error: "Tidak Dapat Update Data" }) 
    }
})
//Delete All Berita
router.delete("/all_berita/:id", async (req, res) => {
    try {
        await All.deleteOne({ _id: req.params.id })
        res.status(204).send() 
    }catch {
        res.status(404)
        res.send({ error: "Data Gagal Di Dalete" }) 
    }
})
// Get One Data All Berita
router.get("/all_berita/:kategori", async (req, res) => { 
    try { 
        const all_berita = await All.find({ kategori: req.params.kategori }) 
        res.send(all_berita) 
    } catch { 
        res.status(404) 
        res.send({ error: "Gagal Menampilkan Salah Satu Data" }) 
    } 
})

//-->TRENDING<--//
// Get Trending Berita
router.get("/trending_berita", isAuthenticated, async (req, res, next) => {
    const trending_berita = await Trending.find()
    res.send(trending_berita)
})
//Post Trending Berita
router.post("/trending_berita", async (req, res) => {
    const trending_berita = new Trending({
        title: req.body.title,
        tanggal: req.body.tanggal,
        gambar: req.body.gambar,
        link: req.body.link
    })
    await trending_berita.save()
    res.send(trending_berita)
})
//Update Trending Berita
router.patch("/trending_berita/:id", async (req, res) => {
    try {
        const trending_berita = await Trending.findOne({ _id: req.params.id })
        if (req.body.title) {
            trending_berita.title = req.body.title
        }
        if (req.body.tanggal) {
            trending_berita.tanggal = req.body.tanggal
        }
        if (req.body.gambar) {
            trending_berita.gambar = req.body.gambar
        }
        if (req.body.link) {
            trending_berita.link = req.body.link
        }
        await trending_berita.save()
        res.send(trending_berita) 
    }catch {
        res.status(404)
        res.send({ error: "Tidak Dapat Update Data" }) 
    }
})
//Delete Trending Berita
router.delete("/trending_berita/:id", async (req, res) => {
    try {
        await Trending.deleteOne({ _id: req.params.id })
        res.status(204).send() 
    }catch {
        res.status(404)
        res.send({ error: "Data Gagal Di Dalete" }) 
    }
})
// Get One Data Trending Berita
router.get("/trending_berita/:id", async (req, res) => { 
    try { 
        const trending_berita = await Trending.findOne({ _id: req.params.id }) 
        res.send(trending_berita) 
    } catch { 
        res.status(404) 
        res.send({ error: "Gagal Menampilkan Salah Satu Data" }) 
    } 
})

//-->NEW<--//
// Get New Berita
router.get("/new_berita", isAuthenticated, async (req, res, next) => {
    const new_berita = await New.find()
    res.send(new_berita)
})
//Post New Berita
router.post("/new_berita", async (req, res) => {
    const new_berita = new New({
        title: req.body.title,
        content: req.body.content,
        gambar: req.body.gambar,
        link: req.body.link
    })
    await new_berita.save()
    res.send(new_berita)
})
//Update New Berita
router.patch("/new_berita/:id", async (req, res) => {
    try {
        const new_berita = await New.findOne({ _id: req.params.id })
        if (req.body.title) {
            new_berita.title = req.body.title
        }
        if (req.body.content) {
            new_berita.content = req.body.content
        }
        if (req.body.gambar) {
            new_berita.gambar = req.body.gambar
        }
        if (req.body.link) {
            new_berita.link = req.body.link
        }
        await new_berita.save()
        res.send(new_berita) 
    }catch {
        res.status(404)
        res.send({ error: "Tidak Dapat Update Data" }) 
    }
})
//Delete New Berita
router.delete("/new_berita/:id", async (req, res) => {
    try {
        await New.deleteOne({ _id: req.params.id })
        res.status(204).send() 
    }catch {
        res.status(404)
        res.send({ error: "Data Gagal Di Dalete" }) 
    }
})
// Get One Data New Berita
router.get("/new_berita/:id", async (req, res) => { 
    try { 
        const new_berita = await New.findOne({ _id: req.params.id }) 
        res.send(new_berita) 
    } catch { 
        res.status(404) 
        res.send({ error: "Gagal Menampilkan Salah Satu Data" }) 
    } 
})

//-->TOP WEEK<--//
// Get Top Week Berita
router.get("/top_week_berita", isAuthenticated, async (req, res, next) => {
    const top_week_berita = await Top_week.find()
    res.send(top_week_berita)
})
//Post Top Week Berita
router.post("/top_week_berita", async (req, res) => {
    const top_week_berita = new Top_week({
        title: req.body.title,
        content: req.body.content,
        gambar: req.body.gambar,
        link: req.body.link
    })
    await top_week_berita.save()
    res.send(top_week_berita)
})
//Update Top Week Berita
router.patch("/top_week_berita/:id", async (req, res) => {
    try {
        const top_week_berita = await Top_week.findOne({ _id: req.params.id })
        if (req.body.title) {
            top_week_berita.title = req.body.title
        }
        if (req.body.content) {
            top_week_berita.content = req.body.content
        }
        if (req.body.gambar) {
            top_week_berita.gambar = req.body.gambar
        }
        if (req.body.link) {
            top_week_berita.link = req.body.link
        }
        await top_week_berita.save()
        res.send(top_week_berita) 
    }catch {
        res.status(404)
        res.send({ error: "Tidak Dapat Update Data" }) 
    }
})
//Delete Top Week Berita
router.delete("/top_week_berita/:id", async (req, res) => {
    try {
        await Top_week.deleteOne({ _id: req.params.id })
        res.status(204).send() 
    }catch {
        res.status(404)
        res.send({ error: "Data Gagal Di Dalete" }) 
    }
})
// Get One Data Top Week Berita
router.get("/top_week_berita/:id", async (req, res) => { 
    try { 
        const top_week_berita = await Top_week.findOne({ _id: req.params.id }) 
        res.send(top_week_berita) 
    } catch { 
        res.status(404) 
        res.send({ error: "Gagal Menampilkan Salah Satu Data" }) 
    } 
})
//router untuk SPA Home
router.get("/",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/index.html'));
})
//router untuk SPA style_trend
router.get("/style_trends",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Style_Trends.html'));
})
//router untuk SPA Look_For_Less
router.get("/look_for_less",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Look_For_Less.html'));
})
//router untuk SPA Hair
router.get("/hair",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Hair.html'));
})
//router untuk SPA Skin
router.get("/skin",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Skin.html'));
})
//router untuk SPA Make_Up
router.get("/make_up",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Make_Up.html'));
})
//router untuk SPA Healty
router.get("/healty",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Healty.html'));
})
//router untuk SPA Working_Life
router.get("/working_life",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Working_Life.html'));
})
//router untuk SPA Inspiration
router.get("/inspiration",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/Inspiration.html'));
})
//router untuk SPA ALL
router.get("/all",  (req, res) => {
	//	res.send('Hello helo dunia!')
		res.sendFile(path.join(__dirname + '/view/All.html'));
})
//router untuk halaman admin dashboard
router.get("/Halaman_admin/",async (req, res) => {
	res.sendFile(path.join(__dirname + '/view/Halaman_admin.html'));
})
//router untuk All Berita Admin
router.get("/All_admin/",async (req, res) => {
	res.sendFile(path.join(__dirname + '/view/All_berita_admin.html'));
})
//router untuk New admin
router.get("/New_admin/",async (req, res) => {
	res.sendFile(path.join(__dirname + '/view/new_admin.html'));
})
//router untuk Top Week Admin
router.get("/Top_admin/",async (req, res) => {
	res.sendFile(path.join(__dirname + '/view/Top_week_admin.html'));
})
//router untuk Trending Admin
router.get("/Trending_admin/",async (req, res) => {
	res.sendFile(path.join(__dirname + '/view/trending_admin.html'));
})
//router untuk Login
router.get("/login", async (req, res) => {
	res.sendFile(path.join(__dirname + '/view/Login.html'));
})
//cek page
//function untuk cek apa boleh akses halaman
router.post("/cek_page", async (req, res) => {
    var old_token= req.body.old_token ;
    jwt.verify(old_token, 'jwtsecret', function(err, decoded){ //jwt melakukan verify
        if (err) { // apa bila ada error
            // res.json({message: 'Halaman Tidak Diijinkan Diakses'}); // jwt melakukan respon
            return res.status(200).json({
                message: 'not_ok'
            })
        }else { 
            return res.status(200).json({
                message: 'ok'
            })
        }
    }) 
})
module.exports = router