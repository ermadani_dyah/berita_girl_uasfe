const express = require("express")
const mongoose = require("mongoose")
const routes = require("./routes")
const path = require('path');
mongoose
.connect("mongodb://localhost:27017/berita_uts",{ 
    useNewUrlParser: true 
})
.then(() => {
    const app = express()
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.json()) // new
    app.use("/uts", routes)
    app.listen(5000, () => {
        console.log("Server sudah Berjalan ")
    })
})